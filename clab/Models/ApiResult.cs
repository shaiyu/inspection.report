﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clab
{ 
    /// <summary>
    /// api的标准返回类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResult<T>
    {
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Msg { get; set; }


        /// <summary>
        /// 携带成功的数据消息
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ApiResult<T> Success()
        {
            return new ApiResult<T>()
            {
                Status = true,
                Msg = "",
            };
        }


        /// <summary>
        /// 携带成功的数据消息
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ApiResult<T> Success(T data)
        {
            return new ApiResult<T>()
            {
                Status = true,
                Msg = "",
                Data = data,
            };
        }

        /// <summary>
        /// 携带成功的数据消息
        /// </summary>
        /// <param name="data"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ApiResult<T> Success(T data, string msg)
        {
            return new ApiResult<T>()
            {
                Status = true,
                Msg = msg,
                Data = data,
            };
        }


        /// <summary>
        /// 携带失败的消息
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ApiResult<T> Failed(string msg)
        {
            return new ApiResult<T>()
            {
                Status = false,
                Msg = msg
            };
        }
    }
}