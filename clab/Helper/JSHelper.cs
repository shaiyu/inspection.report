﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace clab.Helper
{
    public class JSHelper
    {
        public static void ShowAlert(Page page, Type type, string msg)
        {
            var script = $";alert('{msg}');";
            ScriptManager.RegisterStartupScript(page, type, Guid.NewGuid() + "", script, true);
        }

    }
}