﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace clab.Helper
{
    public class MsgHelper
    {
        string website = "http://c.laboratory.hk/";

        //string user = "N0140005";
        //string password = "fgZr1Ne3ohfdb2";
        //string url = "http://smssh1.253.com/msg/send/json";

        static string user = "I7427255";
        static string password = "9F2bCcLlqyc499";
        static string url = "http://intapi.253.com/send/json";


        //设置您要发送的内容：其中“【】”中括号为运营商签名符号，多签名内容前置添加提交
        static string msgContent = "【香港化验所】{0}您好,手机验证码是{1},您正在进行的操作是报告查詢。";

        /// <summary>
        /// 短信新接口 创蓝
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="name"></param>
        public static ApiResult<string> SendSMSCode(string phone, string name, string code)
        {
            //string code = RandomNumHelper.getRandomNum(6);//6位随机数字
            try
            {
                var result = PostMethodToObj(url, JsonConvert.SerializeObject(new
                {
                    account = user,
                    password = password,
                    phone = phone,
                    mobile = phone,
                    report = true,
                    msg = string.Format(msgContent, name, code)
                }));
                JObject jo = (JObject)JsonConvert.DeserializeObject(result);

                return new ApiResult<string>()
                {
                    Status = jo["code"].ToString() == "0",
                    Msg = jo["errorMsg"] + ""
                };
            }
            catch (Exception ex)
            {
                return ApiResult<string>.Failed("系统异常");
            }
        }


        private static string PostMethodToObj(string url, string jsonBody)
        {
            string result = String.Empty;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            // Create NetworkCredential Object 
            NetworkCredential admin_auth = new NetworkCredential("username", "password");

            // Set your HTTP credentials in your request header
            httpWebRequest.Credentials = admin_auth;

            // callback for handling server certificates
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonBody);
                streamWriter.Flush();
                streamWriter.Close();
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            return result;
        }

    }
}