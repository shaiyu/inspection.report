﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clab.Helper
{
    public class RandomNumHelper
    {
        /// <summary>
        /// 获取num位随机数字
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
       public static string getRandomNum(int num)
        {
            string code = String.Empty;
            System.Random random = new Random();
            for(int i = 0; i < num; i++)
            {
                int number = random.Next(10);
                code += number.ToString();
            }

            return code;
        }
    }
}