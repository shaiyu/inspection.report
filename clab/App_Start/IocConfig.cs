﻿using Autofac;
using Autofac.Integration.Web;
using clab.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace clab.App_Start
{
    public class IocConfig
    {
        public static void RegisterDependencies(HttpApplicationState ApplicationState, ref IContainerProvider _containerProvider)
        {
            var builder = new ContainerBuilder();

            //指定注入
            if (DbHelper.GetDbProviderType() == DbHelper.DbProviderType.Mysql)
            {
                builder.RegisterType<ReportMysqlService>().As<IReportService>().InstancePerRequest();
            }
            else
            {
                builder.RegisterType<ReportService>().As<IReportService>().InstancePerRequest();
            }

            //多个实现, 仅会实例化其中一个
            //var assembly = Assembly.GetExecutingAssembly();
            //builder.RegisterAssemblyTypes(assembly)
            //    .Where(t => t.Name.EndsWith("Service"))
            //    .AsImplementedInterfaces();

            var container = builder.Build();
            _containerProvider = new ContainerProvider(container);
        }
    }
}