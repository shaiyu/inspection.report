<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPhone.aspx.cs" Inherits="clab.LoginPhone" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>香港化验所-报告验证</title>
<meta name="keywords" content="香港化验所,报告查询,报告验证,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道" />
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta name="description" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道"/>
<meta http-equiv="Cache-Control" content="must-revalidate,no-cache,no-transform"/>
<meta content="mobiSiteGalore" name="Generator"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
<meta name="format-detection" content="telephone=no"/>
<%--<LINK href="css/css.css" type=text/css rel=stylesheet/>--%>
<script type="text/javascript"> 
 function IsPhone() {    
   var userAgentInfo = navigator.userAgent;    
     var Agents = ["Android", "iPhone",                  "SymbianOS", "Windows Phone",                  "iPad", "iPod"];     
	  var flag = true;    
	    for (var v = 0; v < Agents.length; v++) {        
		  if (userAgentInfo.indexOf(Agents[v]) > 0) {          
		      flag = false;            
			    break;          }      }    
				  return flag;  }  
				  if (IsPhone()) {        
                        //window.location = "http://c.laboratory.hk/";
                    };
</script>
</head>

<body style="overflow:scroll;overflow-x:hidden;overflow-y:hidden; background:#FFFFFF;">
    <table width="350" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#D1E6FA" style=" background:url(http://www.laboratory.hk/w/images/top.jpg) left bottom no-repeat;">
  <tr>
    <td height="120" valign="top"> <div id="div1" style="position:relative; width: 100%; z-index: 0;">
  <div id="div2" style="position:absolute; z-index: 1; width: 100; height: 100; top: 46px; left: 25px;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style=" background:url(http://www.laboratory.hk/w/images/bbg.png);">
      <tr>
        <td height="96"><a href="http://www.laboratory.hk/w"><img src="http://www.laboratory.hk/w/images/logo.png" alt="香港化驗所" width="100" height="100" border="0"></a></td>
      </tr>
    </table>
  </div>
  
  
  <div id="div2" style="position:absolute; z-index: 0; width: 100%; height: 50px; top: 70px; left: 0;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style=" background:url(http://www.laboratory.hk/w/images/hbg.png);">
      <tr>
        <td width="150" height="50"></td>
        <td style=" font-family:'宋体'; font-size:20px;color:#FFFFFF;">香港化驗所</td>
      </tr>
    </table>
  </div>
  
  </div></td>
  </tr>
</table>

<table width="350" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="40" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
</table>

	
	  <table width="350" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
        <td height="96" align="center"><table width="80" border="0" cellpadding="0" cellspacing="0" bgcolor="#E2F4FC">
            <tr>
              <td height="80" align="center" title="有保障的品質"><a href="http://www.laboratory.hk/w/?hklab=1,1,1,4,5,6,2016.html#Quality" target="_blank" class="a3s"><img src="http://www.laboratory.hk/w/images/03.png" alt="有保障的品質" border="0"  style="filter:gray;cursor:pointer"    onmouseover=filters.gray.enabled=0 onmouseout=filters.gray.enabled=1><br>
                   品質
              </a></td>
            </tr>
        </table></td>
        <td align="center"><table width="80" border="0" cellpadding="0" cellspacing="0" bgcolor="#E2F4FC">
            <tr>
              <td width="80" height="80" align="center" title="有保障的品質"><a href="http://www.laboratory.hk/w/?hklab=2,1,2,4,5,6,2016.html" target="_blank" class="a3s"><img src="http://www.laboratory.hk/w/images/05.png" alt="高品質的服務" border="0" style="filter:gray;cursor:pointer"    onmouseover=filters.gray.enabled=0 onmouseout=filters.gray.enabled=1><br>
               服務
              </a></td>
            </tr>
        </table></td>
        <td align="center"><table width="80" border="0" cellpadding="0" cellspacing="0" bgcolor="#E2F4FC">
            <tr>
              <td width="80" height="80" align="center" title="有保障的品質"><a href="http://www.laboratory.hk/w/?hklab=4,1,4,4,5,6,2016.html" target="_blank" class="a3s"><img src="http://www.laboratory.hk/w/images/06.png" alt="優秀的團隊" border="0" style="filter:gray;cursor:pointer"    onmouseover=filters.gray.enabled=0 onmouseout=filters.gray.enabled=1><br>
                   團隊
              </a></td>
            </tr>
        </table></td>
        <td align="center"><table width="80" border="0" cellpadding="0" cellspacing="0" bgcolor="#E2F4FC">
            <tr>
              <td width="80" height="80" align="center" title="有保障的品質"><a href="http://www.laboratory.hk/w/?hklab=6,1,6,4,5,6,2016.html" target="_blank" class="a3s"><img src="http://www.laboratory.hk/w/images/07.png" alt="聯繫我們" border="0" style="filter:gray;cursor:pointer"    onmouseover=filters.gray.enabled=0 onmouseout=filters.gray.enabled=1><br>
                聯絡
              </a></td>
            </tr>
        </table></td>
      </tr>
</table>
    <form id="form1" runat="server">
       <div style=" padding-top:30px;padding-bottom:30px;">
<table width="350" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="152" rowspan="6" align="center" bgcolor="#FFFFFF"><img src="/Asserts/images/lo.jpg" width="139" height="158"></td>
    <td height="36" colspan="2" valign="bottom" bgcolor="#FFFFFF" style=" font-family:微软雅黑; font-size:14px;">受測者姓名</td>
  </tr>
  <tr>
    <td height="30" colspan="2" bgcolor="#FFFFFF">
        <%--<input name="uname"  type="text"  id="uname" style="width:165px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onFocus="this.select(); "   value="张三" maxlength="20"></td>--%>
        <asp:TextBox name="uname" type="text" ID="uname" runat="server" style="width:170px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onfocus="this.select(); " value="张三" maxlength="20" /></td>
                    
   </tr>
  <tr>
    <td height="28" colspan="2" valign="bottom" bgcolor="#FFFFFF" style=" font-family:微软雅黑; font-size:14px;">報告編號</td>
  </tr>
  <tr>
    <td height="30" colspan="2" bgcolor="#FFFFFF">
<%--        <input name="labyear"  type="text"  id="labyear" style="width:40px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onFocus="this.select(); "  value="M17" maxlength="20" >
        <input name="bgnumber"  type="text"  id="bgnumber" style="width:120px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onFocus="this.select(); "  value="K8888" maxlength="20"></td>--%>
         <asp:TextBox name="labyear" type="text" ID="labyear" runat="server" style="width:40px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onfocus="this.select(); " value="M18" maxlength="20" />&nbsp;&nbsp;
         <asp:TextBox name="bgnumber" type="text" ID="bgnumber" runat="server" style="width:120px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onfocus="this.select(); " value="K8888" maxlength="20" /></td>
                    
  </tr>
  <tr>
    <td height="30" colspan="2" bgcolor="#FFFFFF" style=" font-family:微软雅黑; font-size:14px;">驗證碼</td>
  </tr>
  <tr>
    <td width="110" height="35" valign="top" bgcolor="#FFFFFF" style=" font-family:微軟雅黑; font-size:13px;">
        <table width="101" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="53" height="30">
            <%--<input name="yz2"  type="text"  id="yz2" style="width:45px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 14px;font-family: 微软雅黑; font-weight:bold;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px; " onFocus="this.select(); "  maxlength="4" >--%>
            <asp:TextBox name="yz" type="text" ID="yz" runat="server" 
                                style="width:45px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 14px;font-family: 微软雅黑; font-weight:bold;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px; " 
                                onfocus="this.select(); " maxlength="4" />
        </td>
        <td width="48" align="center">
            <img src="ValidateImage.aspx" width="48" height="18" border="0"  style="cursor:hand;" onclick="this.src='ValidateImage.aspx?'+new Date().getTime()" ;="" />
        </td>

      </tr>
    </table></td>
    <td width="88" bgcolor="#FFFFFF">
        <asp:Button Width="68" Height="30" class="input" style=" background:url(/Asserts/images/enter.png)" runat="server" ID="Submit_Logins" OnClick="Submit_Logins_Click" />
        <%--<input type="image" name="Submit2" src="images/enter.png" width="68" height="30">--%>
        <input name="vip" type="hidden" value=""></td>
  </tr>
  <tr>
    <td height="35" colspan="4" align="center" bgcolor="#FFFFFF">您的IP[
        <asp:Label Text="" ID="IP" runat="server"></asp:Label>
        ]<strong><font color="red"></font></strong></td>
  </tr>
</table>

</div>
</form>
</body>
</html>
