﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportDetail.aspx.cs" Inherits="clab.View.ReportDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>

    <script language="javascript">

        if (document.all) {
            document.onselectstart = function () { return false; }; //for ie 
        } else {
            document.onmousedown = function () { return false; };
            document.onmouseup = function () { return true; };
        }
        document.onselectstart = new Function('event.returnValue=false;');
    </script>

    <div style="-moz-user-select: none; -webkit-user-select: none; user-select: none; padding-top: 20px;">
        <%-- Header --%>
        <table width="100%" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td height="31" valign="top" style="padding-left: 20px; font-family: 微软雅黑; font-size: 18px">查詢結果如下

                        <div id="div5" style="position: relative; width: 100%; z-index: 10;">
                            <div id="layer12" style="position: absolute; z-index: 10; top: -56px; border: 1px none #000000; right: 10px; width: 71px; height: 54px;">
                                <table width="100%" height="53" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="right"><a href="http://www.laboratory.hk/">
                                                <img src="/Asserts/images/close.png" alt="點擊退出查詢" border="0"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <div id="layer14" style="position: absolute; z-index: 10; top: 24px; left: -4px; border: 1px none #000000; width: 258px; height: 89px;">
                                <table width="100%" height="53" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img src="/Asserts/images/logos.png" alt="點擊退出查詢" width="197" border="0"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <%-- Content --%>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
            <tbody>
                <tr>
                    <td height="500" align="center" bgcolor="#FFFFFF" style="padding: 10px; font-family: 微软雅黑; color: #666666; font-size: 15px; line-height: 140%;">


                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td width="211" height="3" align="center" bgcolor="#1366A9"></td>
                                    <td width="739" height="3" align="center" bgcolor="#2095D4"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td height="20" valign="middle">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td height="87" align="center" valign="middle" style="font-family: Arial; font-size: 25px; color: #000000; font-weight: bold; padding-left: 20px; background: url(images/libar.png) no-repeat left;">Maternal Blood Screening Report</td>
                                </tr>
                            </tbody>
                        </table>


                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: #000000  double 1px;">
                            <tbody>
                                <tr>
                                    <td height="27" valign="middle" style="font-family: 微软雅黑; font-size: 20px; color: #000000; font-weight: bold; padding-left: 10px;">Patient Information</td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td width="475" height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Test Requester / 受測者: <asp:Label runat="server" ID="txUserName"></asp:Label>
                                    </td>
                                    <td width="475" height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Referring Clinic / 轉介診所: <asp:Label runat="server" ID="txReferring"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">ID No. / 證件號碼: <asp:Label runat="server" ID="txIdNumber"></asp:Label></td>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Clinic Ref No. / 轉介編號: <asp:Label runat="server" ID="txClinic"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">D.O.B / 出生日期: <asp:Label runat="server" ID="txAge"></asp:Label></td>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Report No. / 報告編號: <asp:Label runat="server" ID="txLabId"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Gestational Age / 妊娠期: <asp:Label runat="server" ID="txGWeek"></asp:Label></td>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Specimen / 樣本類別: <asp:Label runat="server" ID="txType"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Pregnancy / 妊娠: <asp:Label runat="server" ID="txPregnancy"></asp:Label></td>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Sample Collection Date / 採樣日期: <asp:Label runat="server" ID="txCdate"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Ethnic Origin / 種族: <asp:Label runat="server" ID="txEthnic"></asp:Label></td>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Sample Received Date / 收樣日期: <asp:Label runat="server" ID="txRdate"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">&nbsp;</td>
                                    <td height="32" valign="middle" style="font-family: 微软雅黑; font-size: 16px; color: #000000; padding-left: 10px;">Reporting Date / 報告日期: <asp:Label runat="server" ID="txIdate"></asp:Label></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#FF0000">
                            <tbody>
                                <tr>
                                    <td height="152" align="center" bgcolor="#FFFFFF" style="padding: 20px; font-family: 微软雅黑; font-size: 20px; letter-spacing: 4px; line-height: 180%;">為確保您的個人隱私安全, 查詢結果暫不全部顯示,<br>
                                        報告編號[ <font color="red"> <asp:Label runat="server" ID="txLabId2"></asp:Label> </font>] 真實有效<br>
                                        香港化驗所保留最終解釋權
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background: url(/Asserts/images/bottom.png) no-repeat right bottom;">
                            <tbody>
                                <tr>
                                    <td height="58" valign="bottom" style="padding-left: 15px; font-family: Arial; font-size: 12px;">
                                        <p>
                                            HKL/ML/RP-1 (02/10/2017)<br>
                                            <br>
                                            <em>Address: Unit F, 8/F., Valiant Industrial Centre, 2-12 Au Pui Wan Street, Fo Tan, N. T., Hong Kong</em>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="19" valign="middle" style="padding-left: 10px;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="21" align="left">
                                                        <img src="/Asserts/images/tels.png" width="21" height="19"></td>
                                                    <td style="font-family: Arial; font-size: 12px; color: #4aa2db;">電話 (+852) 2350-6338 </td>
                                                    <td width="21">
                                                        <img src="/Asserts/images/faxx.png" width="21" height="19"></td>
                                                    <td style="font-family: Arial; font-size: 12px; color: #4aa2db;">傳真 (+852) 3468-3495</td>
                                                    <td width="16">
                                                        <img src="/Asserts/images/mail.png" width="21" height="19"></td>
                                                    <td style="font-family: Arial; font-size: 12px; color: #4aa2db;">電郵 info@laboratory.hk</td>
                                                    <td width="16">
                                                        <img src="/Asserts/images/web.png" width="21" height="19"></td>
                                                    <td style="font-family: Arial; font-size: 12px; color: #4aa2db;">網址 <a href="http://www.laboratory.hk/" target="_blank" style="color: #4aa2db; text-decoration: none;">http://www.laboratory.hk/</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
