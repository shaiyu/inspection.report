<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="clab.Login" %>
<html>
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>香港化验所-报告验证</title>
<meta name="keywords" content="香港化验所,报告查询,报告验证,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道"/>
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta name="description" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道"/>
<meta http-equiv="Cache-Control" content="must-revalidate,no-cache,no-transform"/>
<meta content="mobiSiteGalore" name="Generator"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
<meta name="format-detection" content="telephone=no"/>
    <link href="Asserts/CSS/common.css" rel="stylesheet" />
    <script type="text/javascript"> 
        function IsPhone() {
            var userAgentInfo = navigator.userAgent;
            var Agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
            var flag = false;
            for (var v = 0; v < Agents.length; v++) {
                if (userAgentInfo.indexOf(Agents[v]) > 0) {
                    flag = true;
                    break;
                }
            }
            return flag;
        }
        if (IsPhone()) {
            window.location = "http://c.laboratory.hk/LoginPhone.aspx";
        };
</script>
</head>
<body style="overflow:scroll;overflow-x:hidden;overflow-y:hidden; background:url(/Asserts/images/bbbg.png) center;">


<table width="838" height="631" border="0" align="center" cellpadding="0" cellspacing="0" style=" background:url(/Asserts/images/hklabbg.png) right no-repeat;">
      <tbody><tr>
        <td height="485">

<table width="670" height="310" border="0" align="center" cellpadding="0" cellspacing="0" style=" background:url(/Asserts/images/login.png) no-repeat center top;">
 <form id="UserReg" runat="server">
            <tbody><tr>
              <td width="523" height="310" valign="middle"><table width="441" border="0" cellpadding="0" cellspacing="0">
                  <tbody><tr>
                    <td width="237" height="36">&nbsp;</td>
                    <td colspan="2" valign="bottom" style=" font-family:微软雅黑; font-size:14px;">受測者姓名</td>
                    <td width="24">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="30">&nbsp;</td>
                    <td colspan="2">
                         <asp:TextBox name="uname" type="text" ID="uname" runat="server" style="width:170px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onfocus="this.select(); " value="张三" maxlength="20" /></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="28">&nbsp;</td>
                    <td colspan="2" valign="bottom" style=" font-family:微软雅黑; font-size:14px;">報告編號</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="30">&nbsp;</td>
                    <td colspan="2">
                         <asp:TextBox name="labyear" type="text" ID="labyear" runat="server" style="width:40px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onfocus="this.select(); " value="M18" maxlength="20" />&nbsp;&nbsp;
                       <asp:TextBox name="bgnumber" type="text" ID="bgnumber" runat="server" style="width:120px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 13px;font-family:微软雅黑;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px;padding-top:2px;" onfocus="this.select(); " value="K8888" maxlength="20" /></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="30">&nbsp;</td>
                    <td colspan="2" style=" font-family:微软雅黑; font-size:14px;">驗證碼</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="35">&nbsp;</td>
                    <td width="112" valign="top" style=" font-family:微軟雅黑; font-size:13px;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tbody><tr>
                        <td width="47" height="30">
                            <asp:TextBox name="yz" type="text" ID="yz" runat="server" 
                                style="width:45px; BORDER-RIGHT: #0f4888 1px solid; BORDER-TOP: #0f4888 1px solid; FONT-SIZE: 14px;font-family: 微软雅黑; font-weight:bold;  BORDER-LEFT: #0f4888 1px solid; BORDER-BOTTOM: #0f4888 1px solid; HEIGHT: 25px; " 
                                onfocus="this.select(); " maxlength="4" />
                                
                        </td>
                        <td width="41" align="center">
                            <img src="ValidateImage.aspx" width="50" height="23" border="0" style="cursor:hand;" onclick="this.src='ValidateImage.aspx?'+new Date().getTime()" ;="" />
                        </td>
                      </tr>
                    </tbody></table></td>
                    <td width="68">
                     <%--   <asp:Button type="image"  src="/Asserts/images/images/enter.png" width="68" height="30" runat="server" ID="Submit_Login" OnClick="Submit_Login_Click" />--%>
                        <asp:Button Width="68" Height="30" class="input" style=" background:url(/Asserts/images/enter.png)" runat="server" ID="Submit_Logins" OnClick="Submit_Logins_Click" />
                        <input name="vip" type="hidden" value=""></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="35" colspan="4" align="center">您的IP <asp:Label Text="" ID="IP" runat="server"></asp:Label>
                        <strong><font color="red"></font></strong></td>
                </tr>
              </tbody></table></td>
            </tr>
        
        </tbody>
     </form> 
     </table>		
		
		</td>
      </tr>
</tbody>

</table>
</body>
</html>
