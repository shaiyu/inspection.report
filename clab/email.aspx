<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="email.aspx.cs" Inherits="clab.email" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<%--<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>--%>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>香港化验所-补全邮箱</title>
<meta name="keywords" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道"/>
<meta name="MSSmartTagsPreventParsing" content="TRUE" />
<meta name="description" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道"/>
<link href="/css/css.css" type="text/css" rel="stylesheet"/>
<meta http-equiv="Cache-Control" content="must-revalidate,no-cache,no-transform"/>
<meta content="mobiSiteGalore" name="Generator"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="apple-touch-fullscreen" content="YES"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
<meta name="format-detection" content="telephone=no"/>
</head>
<body style="overflow:scroll;overflow-x:hidden;">
    <form action="" method="post" name="form" onsubmit="return checkform();" runat="server">
<table width="350" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td height="100%"><table width="326" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#B1CEED" style="border-bottom: #75C4F0 dotted 1px; border-left:#75C4F0 dotted 1px; border-right: #75C4F0 dotted 1px; border-top: #75C4F0 dotted 1px;">

        <tbody><tr>
          <td height="92" colspan="2" align="center" bgcolor="#FFFFFF"><img src="/images/logo_cms.png" alt="Laboratory" width="248" height="91"></td>
        </tr> 
		       <tr>
          <td height="35" align="center" bgcolor="#FFFFFF" style="font-family:微软雅黑; font-size:16px; color:#0099FF;">報告編號</td>
          <td height="35" bgcolor="#FFFFFF" style="padding-left:10px; font-family:Arial;font-size:16px; color:#0099FF;">M18K2848<input name="vv_ag" type="hidden" value="K2848"></td>
        </tr>
        <tr>
          <td width="107" height="35" align="center" bgcolor="#FFFFFF" style="font-family:微软雅黑; font-size:16px; color:#0099FF;">姓名</td>
          <td width="214" height="35" bgcolor="#FFFFFF" style="padding-left:10px; font-family:微软雅黑;font-size:16px; color:#0099FF;">肖梅<input name="vv_kh" type="hidden" value="肖梅"></td>
        </tr>
		
        <tr>
          <td height="35" align="center" bgcolor="#FFFFFF" style="font-family:微软雅黑; font-size:16px; color:#0099FF;">郵箱</td>
          <td width="214" height="35" bgcolor="#FFFFFF" style="padding-left:10px; font-family:Arial;">

            <input name="kh_email" type="text" id="kh_email" value="" style="font-family:Arial;color:#0099FF;font-size:16px; width:150px;">*<input name="action" type="hidden" value="love"></td>
        </tr>		
	
        <tr>
          <td height="39" colspan="2" align="center" bgcolor="#FFFFFF">
		  
          <asp:Button Width="68" Height="30" style="font-family:微软雅黑;font-size: 14px;" Text="提交" runat="server" ID="submit" OnClick="submit_Click" />
		  </td>
        </tr>
      
    </tbody></table></td>
  </tr>
</tbody></table>



        </form>
</body>
</html>
