﻿using clab.Service;
using EasyJsonToSql;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace clab.View
{
    public partial class ReportDetail : System.Web.UI.Page
    {
        public IReportService _reportService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var id = Session["id"];
            //var id = 95003;

            var model = _reportService.Get(id + "");
            if (model != null)
            {
                txAge.Text = model.age;
                txCdate.Text = model.cdate;
                txClinic.Text = model.clinic;
                txEthnic.Text = model.Ethnic;
                txGWeek.Text = model.gweek + " wks";
                txIdate.Text = model.idate;
                txIdNumber.Text = model.idnumber;
                txLabId.Text = txLabId2.Text = model.labyear + model.labid;
                txPregnancy.Text = model.Pregnancy;
                txRdate.Text = model.rdate;
                txReferring.Text = model.referring;
                txType.Text = model.type;
                txUserName.Text = model.username;
            }
            else
            {
                //查询失败
                Response.Redirect("/Login.aspx");
            }
        }
    }
}