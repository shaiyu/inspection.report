<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tel.aspx.cs" Inherits="clab.tel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <title>香港化验所-补全手机</title>
    <meta name="keywords" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道" />
    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
    <meta name="description" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道" />
    <meta http-equiv="Cache-Control" content="must-revalidate,no-cache,no-transform" />
    <meta content="mobiSiteGalore" name="Generator" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="apple-touch-fullscreen" content="YES" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <style>
        html, body, form{
            height: 100%;
        }
    </style>
    
    <link href="Asserts/CSS/common.css" rel="stylesheet" />
</head>
<body style="overflow: scroll; overflow-x: hidden; background-color: #F7F7F7; height: 100%; overflow-y: hidden;">
        <form  name="form" runat="server">
            <table width="970" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td height="100%">
                            <table width="288" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#B1CEED" style="border-bottom: #75C4F0 dotted 1px; border-left: #75C4F0 dotted 1px; border-right: #75C4F0 dotted 1px; border-top: #75C4F0 dotted 1px;">
                            
                                <tbody>
                                    <tr>
                                        <td height="114" colspan="2" align="center" bgcolor="#FFFFFF">
                                            <img src="/Asserts/images/logo_cms.png" alt="Laboratory" width="248" height="91"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#FFFFFF" style="font-family: 微软雅黑; font-size: 16px; color: #0099FF;">報告編號</td>
                                        <td height="30" bgcolor="#FFFFFF" style="padding-left: 10px; font-family: Arial; font-size: 16px; color: #0099FF;">
                                            <asp:Label ID="vv_ag" runat="server" Text=" M18K2848"></asp:Label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="107" align="center" bgcolor="#FFFFFF" style="font-family: 微软雅黑; font-size: 16px; color: #0099FF;">姓名</td>
                                        <td width="214" height="30" bgcolor="#FFFFFF" style="padding-left: 10px; font-family: 微软雅黑; font-size: 16px; color: #0099FF;">
                                            <asp:Label ID="vv_kh" runat="server" Text="肖梅"></asp:Label>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="center" bgcolor="#FFFFFF" style="font-family: 微软雅黑; font-size: 16px; color: #0099FF;">手機</td>
                                        <td width="214" height="30" bgcolor="#FFFFFF" style="padding-left: 10px; font-family: Arial;">
                                            <asp:DropDownList runat="server" ID="ddl_area"  Style="font-family: Arial; color: #0099FF; font-size: 12px; padding: 2px 0;width: 50px;">
                                                <asp:ListItem Text="内地 (86)" Value="86" />
                                                <asp:ListItem Text="香港 (00852)" Value="00852" />
                                                <asp:ListItem Text="澳门 (00853)" Value="00853" />
                                            </asp:DropDownList>
                                            <asp:TextBox runat="server" ID="kh_tel" Style="font-family: Arial; color: #0099FF; font-size: 16px; width: 100px;"></asp:TextBox>
                                            *
                                        </td>
                                    </tr>

                                    <tr>
                                        <td height="39" colspan="2" align="center" bgcolor="#FFFFFF">
                                            <asp:Button ID="Submit" Text="提交" Style="font-family: 微软雅黑; font-size: 14px;" runat="server" OnClick="Submit_Click" />
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

        </form>

</body>
</html>
