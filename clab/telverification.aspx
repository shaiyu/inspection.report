<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="telverification.aspx.cs" Inherits="clab.telverification" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <title>香港化验所-补全手机</title>
    <meta name="keywords" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道" />
    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
    <meta name="description" content="香港化验所,报告查询,查询报告,化验所查询报告,化验所报告查询,香港化验所查报告,化验报告查询,查询报告通道" />
    <meta http-equiv="Cache-Control" content="must-revalidate,no-cache,no-transform" />
    <meta content="mobiSiteGalore" name="Generator" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="apple-touch-fullscreen" content="YES" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <script type="text/javascript" src="http://c.laboratory.hk/Asserts/Plug/js/jquery.min.js"></script>
    <script type="text/javascript" src="http://c.laboratory.hk/Asserts/Plug/js/verify.js"></script>
    <link rel="stylesheet" type="text/css" href="http://c.laboratory.hk/Asserts/Plug/css/verify.css" />
    <style>
        html, body, form {
            height: 100%;
        }
    </style>
    <link href="Asserts/CSS/common.css" rel="stylesheet" />
</head>
<body style="overflow: scroll; overflow-x: hidden; height: 100%; margin: auto; overflow-y: hidden;">
    <form name="form" runat="server">
        <table width="350" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td height="100%">
                        <table width="326" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#B1CEED" style="border-bottom: #75C4F0 dotted 1px; border-left: #75C4F0 dotted 1px; border-right: #75C4F0 dotted 1px; border-top: #75C4F0 dotted 1px;">
                            <tbody>
                                <tr>
                                    <td height="92" colspan="2" align="center" bgcolor="#FFFFFF">
                                        <img src="/Asserts/images/logo_cms.png" alt="Laboratory" width="248" height="91"></td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#FFFFFF" style="font-family: 微软雅黑; font-size: 16px; color: #0099FF;">報告編號</td>
                                    <td height="30" bgcolor="#FFFFFF" style="padding-left: 10px; font-family: Arial; font-size: 16px; color: #0099FF;">
                                        <asp:TextBox ID="value_code" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Label ID="vv_ag" runat="server" Text=" M18K2848"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="107" align="center" bgcolor="#FFFFFF" style="font-family: 微软雅黑; font-size: 16px; color: #0099FF;">姓名</td>
                                    <td width="214" height="30" bgcolor="#FFFFFF" style="padding-left: 10px; font-family: 微软雅黑; font-size: 16px; color: #0099FF;">

                                        <asp:Label ID="vv_kh" runat="server" Text="肖梅"></asp:Label>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" bgcolor="#FFFFFF" style="font-family: 微软雅黑; font-size: 16px; color: #0099FF;">手機</td>
                                    <td width="214" height="30" bgcolor="#FFFFFF" style="padding-left: 10px; font-family: Arial;">
                                        <asp:Label runat="server" ID="phone" Text=""></asp:Label>

                                    </td>
                                </tr>

                                <tr>
                                    <td height="39" align="center" bgcolor="#FFFFFF" style="font-family: 微软雅黑; font-size: 16px; color: #0099FF;">验证码</td>
                                    <td height="39" bgcolor="#FFFFFF" style="padding-left: 10px; font-family: Arial;">
                                        <asp:TextBox name="khyzm" type="text" Style="font-family: Arial; color: #0099FF; font-size: 16px; width: 150px;" ID="khyzm" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="39" colspan="2" align="center" bgcolor="#FFFFFF">
                                        <input type="button" value="沒有收到驗證碼？" style="border: 0px none; color: #666666; background: #ffffff; cursor: pointer;" id="nocode" onclick="noaccept_click()" />
                                    </td>

                                </tr>
                                <tr>
                                    <td height="39" colspan="2" align="center" bgcolor="#FFFFFF">
                                        <asp:Button ID="Submit" Text="提交" Style="font-family: 微软雅黑; font-size: 14px;" runat="server" OnClick="Submit_Click" />
                                        <asp:Button ID="Next" Text="下一步" Style="font-family: 微软雅黑; font-size: 14px; display: none;" runat="server" OnClick="Next_Click" />
                                    </td>
                                </tr>

                            </tbody>

                        </table>
                    </td>
                </tr>
            </tbody>

        </table>
    </form>
    <div id="dialog" style="position: absolute; display: none;">
        <div id="mpanel4" style="position: relative;"></div>
        <script>
            $('#mpanel4').slideVerify({
                type: 2,		//类型
                vOffset: 5,	//误差量，根据需求自行调整
                vSpace: 5,	//间隔
                explain: '',
                imgUrl: '/Asserts/Plug/images/',
                imgName: ['1.jpg', '2.jpg','3.jpg', '4.jpg','5.jpg', '6.jpg','7.jpg'],
                imgSize: {
                    width: '300px',
                    height: '200px',
                },
                blockSize: {
                    width: '40px',
                    height: '40px',
                },
                barSize: {
                    width: '300px',
                    height: '40px',
                },
                ready: function () {
                },
                success: function () {
                    var mySubmit = document.getElementById("Next");
                    mySubmit.style.display = "block";
                    setTimeout(hidenoAccept, 3000);

                },
                error: function () {
                    //		        	alert('验证失败！');
                }

            });

            function hidenoAccept() {
                $('#dialog').hide();
            }
            function noaccept_click() {
                if ($('#dialog').is(':hidden')) {
                    var $btn = $('#nocode');
                    $('#dialog').css("top", $btn.offset().top - $('#dialog').height())
                    $('#dialog').css("left", $btn.offset().left)//+ 120
                    $('#dialog').show();
                    var mySubmit = document.getElementById("Submit");
                    mySubmit.style.display = "none";
                } else {
                    hidenoAccept();
                }
            }
        </script>
    </div>

</body>
</html>

