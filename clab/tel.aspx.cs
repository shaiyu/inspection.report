﻿using clab.Helper;
using clab.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace clab
{
    public partial class tel : System.Web.UI.Page
    {
        public IReportService _reportService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string uname = Session["username"]==null? Request["uname"] ==null?"" : HttpUtility.UrlDecode(Request["uname"], Encoding.UTF8) : Session["username"].ToString();
            string bgnumber = Session["bgnumber"]==null? Request["bgnumber"] ==null?"": Request["bgnumber"] : Session["bgnumber"].ToString();
            string labyear = Session["labyear"]==null? Request["labyear"]==null?"": Request["labyear"] : Session["labyear"].ToString();

            vv_kh.Text = uname;
            vv_ag.Text = labyear + bgnumber;
            Session["username"] = vv_kh.Text;
            Session["bgnumber"] = bgnumber;
            Session["labyear"] = labyear;
            //Session["phone"] = result.tel;
            var result = _reportService.GetByLabYearIdUserName(labyear, bgnumber, uname);
            if (result != null)
            {
                Session["username"] = uname;
                Session["bgnumber"] = bgnumber;
                Session["labyear"] = labyear;
                Session["phone"] = result.tel;

                if (result.tel == "" || result.tel == null)
                {
                   
                }
                else
                {
                    if (result.khyzm!=null&&result.khyzm != "")
                    {
                        Response.Redirect("telverification.aspx");
                    }
                    else
                    {
                        //getSMSCode(result.tel);
                        sendSMSCode(result.tel, vv_kh.Text);
                        Response.Redirect("telverification.aspx");
                    }
                }
            }
         }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (kh_tel.Text.Length != 11)
            {
                Response.Write("<script>alert('您输入的手机号不正确！')</script>");
                return;
            }

            var name = vv_kh.Text;
            var phone = ddl_area.SelectedValue + kh_tel.Text; // 86 + 159XXXXXXXXX
            var result = _reportService.GetByPhone(phone);
            if (result != null)
            {
                Response.Write("<script>alert('你输入的手机已经存在，请重新输入！');location.href='javascript:history.go(-1);'; </script>");
            }
            else
            {
                string bgnumber = Session["bgnumber"] == null ? Request["bgnumber"] == null ? "" : Request["bgnumber"] : Session["bgnumber"].ToString();
                
                var success = _reportService.UpdatePhone(bgnumber, name, phone);
                if (success)
                {
                    Response.Write("<script>alert('手機號碼更新成功！');</script>");
                    Session["phone"] = phone;
                    sendSMSCode(phone, name);
                    Response.Redirect("telverification.aspx");
                }
                else
                {
                    Response.Write("<script>alert('手機號碼更新失败！');");
                }

            }
        }

        /// <summary>
        /// 短信新接口 创蓝
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="name"></param>
        public void sendSMSCode(string phone, string name)
        {
            string code = RandomNumHelper.getRandomNum(6);//6位随机数字
            Session["code"] = code;
            var msgSend = MsgHelper.SendSMSCode(phone, name, code);
            if (msgSend.Status)
            {
                string bgnumber = Session["bgnumber"] == null ? Request["bgnumber"] == null ? "" : Request["bgnumber"] : Session["bgnumber"].ToString();
                _reportService.UpdateVerifyCode(bgnumber, name, code);
                Response.Write("<script>alert('驗證碼已經發送！');</script>");
            }
            else
            {
                Response.Write("<script>alert('" + msgSend.Msg  + ", 驗證碼发送失败，请联系管理员！！');</script>");
            }
        }
    }
}