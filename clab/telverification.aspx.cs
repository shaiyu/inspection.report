﻿using clab.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace clab
{
    public partial class telverification : System.Web.UI.Page
    {
        public IReportService _reportService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string uname = Session["username"]==null?"":Session["username"].ToString();
            string bgnumber = Session["bgnumber"]==null?"":Session["bgnumber"].ToString();
            string labyear = Session["labyear"]==null?"":Session["labyear"].ToString();
            string tel = Session["phone"] == null ? "" : Session["phone"].ToString();
            if (tel == "")
            {

            }
            else
            {
                phone.Text = tel;
            }
            vv_kh.Text = uname;
            vv_ag.Text = labyear + bgnumber;
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            string code = khyzm.Text;
            string username = vv_kh.Text;
            string bgnumber = Session["bgnumber"] == null ? "" : Session["bgnumber"].ToString();

            var result = _reportService.GetByLabIdUserName(bgnumber, username);
            if (result != null && result.khyzm != null)
            {
                if (result.khyzm == code)
                {
                    Session["Id"] = result.id;

                    _reportService.UpdateVerifyCode(bgnumber, username, "");
                    Response.Redirect("ReportDetail.aspx");
                }
                else
                {
                    Response.Write("<script>alert('驗證碼错误，请重新输入！');</script>");
                   
                }
            }
        }


        protected void Next_Click(object sender, EventArgs e)
        {
            string username = vv_kh.Text;
            string bgnumber = Session["bgnumber"] == null ? "" : Session["bgnumber"].ToString();

            var result = _reportService.GetByLabIdUserName(bgnumber, username);
            if (result != null)
            {
                Session["Id"] = result.id;
                Response.Redirect("ReportDetail.aspx");
            }
        }
        public void getCode()
        {
            string username = vv_kh.Text;
            string bgnumber = Session["bgnumber"] == null ? "" : Session["bgnumber"].ToString();
            var result = _reportService.GetByLabIdUserName(bgnumber, username);
            if (result != null)
            {
                Session["Id"] = result.id;
                Response.Redirect("ReportDetail.aspx");
            }
        }
    }
}