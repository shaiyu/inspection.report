﻿using EasyJsonToSql;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace clab.Service
{
    /// <summary>
    /// 使用msql数据库
    /// </summary>
    public class ReportMysqlService : IReportService
    {
        public dynamic Get(string id)
        {
            var sql = @" select * from Report where Id = @id ";
            var data = DbHelper.GetModel<dynamic>(sql, new DbField("id", id));
            return Adapter(data);
        }

        public dynamic GetByPhone(string phone)
        {
            var sql = @" select * from Report where Phone = @phone ";
            var data = DbHelper.GetModel<dynamic>(sql, new DbField("phone", phone));
            return Adapter(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="labyear"></param>
        /// <param name="labid"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public dynamic GetByLabYearIdUserName(string labyear, string labid, string username) {
            var sql = $@" select * from Report where Lab_Nos='{labid}' and TestRecipient='{username}' and Lab_no='{labyear}' ";
            var data = DbHelper.GetModel<dynamic>(sql);
            return Adapter(data);
        }

        public dynamic GetByLabIdUserName(string labid, string username)
        {
            var sql = $@" select * from Report where Lab_Nos='{labid}' and TestRecipient='{username}' ";
            var data = DbHelper.GetModel<dynamic>(sql);
            return Adapter(data);
        }


        public bool UpdateVerifyCode(string labid, string username, string verifyCode) {
            var sqlUpdate = $@" update Report set VerifyCode='{verifyCode}' where TestRecipient='{username}' and Lab_Nos='{labid}'";
            return DbHelper.Execute(sqlUpdate) > 0;
        }


        public bool UpdatePhone(string labid, string username, string phone)
        {
            var sqlUpdate = $@" update Report set Phone='{phone}' where TestRecipient='{username}' and Lab_Nos='{labid}' ";
            return DbHelper.Execute(sqlUpdate) > 0;
        }


        readonly Dictionary<string, string> DicCols = new Dictionary<string, string>() {
            { "Id", "id" },
            { "Lab_no", "labyear" },//报告编号年份
            { "Lab_Nos", "labid" },//报告编号
            { "ClinicRefNo", "clinic" },//转介编号
            { "ReferringPhysician", "referring" },//转介诊所/医生
            { "TestRecipient", "username" },//受测者姓名
            { "DateofBirth", "age" },//出生日期
            { "IDNo", "idnumber" },//证件号码
            { "GestationalAge", "gweek" },//怀孕周期
            { "NumberofFetus", "Pregnancy" },//妊娠 客户怀孕胚胎数（单双胞胎）
            { "EthnicOrigin", "Ethnic" },//种族
            { "SpecimentType", "type" },//样本类别
            { "CollectDate", "cdate" },//采样时间
            { "ReceviedDate", "rdate" },//收样日期
            { "ReportDate", "idate" },//报告日期
            { "TestResults", "Report" },//化验结果
            { "Locus", "Conclusion" },//结果明细 检测结果匹配数（成功数）(TestResults=D Locus>0, TestResults=ND Locus=0,TestResults=R Locus无值)
            //{ "", "pic" },//扫描件
            { "CreatedTime", "addtime" },//填表时间
            { "Phone", "tel" },//客人电话
            { "VerifyCode", "khyzm" },//客户验证码
            { "Email", "email" },//email
        };

        public dynamic Adapter(dynamic data) {
            if (data == null)
            {
                return data;
            }

            IDictionary<string, object> dic = data;//upper
            //dic.Add("Age", a.age);
            //dic.Remove("age");

            IDictionary<string, object> newData = new ExpandoObject(); 
            foreach (var key in DicCols.Keys)
            {
                var newKey = DicCols[key];
                if (dic.ContainsKey(key))
                {
                    newData.Add(newKey, dic[key]);
                }
                else
                {
                    newData.Add(newKey, null);
                }
            }
            return newData;
        }
    }
}