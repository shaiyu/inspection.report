﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clab.Service
{
    public interface IReportService
    {
        dynamic Get(string id);

        dynamic GetByPhone(string phone);

        dynamic GetByLabIdUserName(string labid, string username);

        dynamic GetByLabYearIdUserName(string labyear, string labid, string username);

        bool UpdateVerifyCode(string labid, string username, string verifyCode);

        bool UpdatePhone(string labid, string username, string phone);
    }
}