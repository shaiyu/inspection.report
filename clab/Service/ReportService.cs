﻿using EasyJsonToSql;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace clab.Service
{
    /// <summary>
    /// 使用access数据库
    /// </summary>
    public class ReportService : IReportService
    {
        public dynamic Get(string id)
        {
            var sql = @" select * from khbg where id = @id ";
            return DbHelper.GetModel<dynamic>(sql, new DbField("id", id));
        }

        public dynamic GetByPhone(string phone)
        {
            var sql = $@" select * from khbg where tel='{phone}' ";
            return DbHelper.GetModel<dynamic>(sql);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="labyear"></param>
        /// <param name="labid"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public dynamic GetByLabYearIdUserName(string labyear, string labid, string username) {
            var sql = $@" select * from khbg where labid='{labid}' and username='{username}' and labyear='{labyear}' ";
            var data = DbHelper.GetModel<dynamic>(sql);
            return Adapter(data);
        }

        public dynamic GetByLabIdUserName(string labid, string username)
        {
            var sql = $@" select * from khbg where labid='{labid}' and username='{username}' ";
            var data = DbHelper.GetModel<dynamic>(sql);
            return Adapter(data);
        }


        public bool UpdateVerifyCode(string labid, string username, string verifyCode) {
            var sqlUpdate = $@" update khbg set khyzm='{verifyCode}' , addtime ='{DateTime.Now.ToString()}' where username='{username}' and labid='{labid}'";
            return DbHelper.Execute(sqlUpdate) > 0;
        }

        public bool UpdatePhone(string labid, string username, string phone)
        {
            var sqlUpdate = $@" update khbg set tel='{phone}' , addtime ='{DateTime.Now.ToString()}'  where username='{username}' and labid='{labid}' ";
            return DbHelper.Execute(sqlUpdate) > 0;
        }


        public dynamic Adapter(dynamic data) {

            IDictionary<string, object> dic = data;//upper
            //dic.Add("Age", a.age);
            //dic.Remove("age");
            return data;
        }

    }
}