<%@ Language="VBScript" %>
<!--#include file="vip/admin.asp"-->



<%
'不使用输出缓冲区，直接将运行结果显示在客户端
Response.Buffer = False

'声明待检测数组
Dim ObjTotest(26,4)

ObjTotest(0,0) = "MSWC.AdRotator"
ObjTotest(1,0) = "MSWC.BrowserType"
ObjTotest(2,0) = "MSWC.NextLink"
ObjTotest(3,0) = "MSWC.Tools"
ObjTotest(4,0) = "MSWC.Status"
ObjTotest(5,0) = "MSWC.Counters"
ObjTotest(6,0) = "IISSample.ContentRotator"
ObjTotest(7,0) = "IISSample.PageCounter"
ObjTotest(8,0) = "MSWC.PermissionChecker"
ObjTotest(9,0) = "Scripting.FileSystemObject"
	ObjTotest(9,1) = "(FSO 文本文件读写)"
ObjTotest(10,0) = "adodb.connection"
	ObjTotest(10,1) = "(ADO 数据对象)"
	
ObjTotest(11,0) = "SoftArtisans.FileUp"
	ObjTotest(11,1) = "(SA-FileUp 文件上传)"
ObjTotest(12,0) = "SoftArtisans.FileManager"
	ObjTotest(12,1) = "(SoftArtisans 文件管理)"
ObjTotest(13,0) = "LyfUpload.UploadFile"
	ObjTotest(13,1) = "(文件上传组件)"
ObjTotest(14,0) = "Persits.Upload.1"
	ObjTotest(14,1) = "(ASPUpload 文件上传)"
ObjTotest(15,0) = "w3.upload"
	ObjTotest(15,1) = "(Dimac 文件上传)"
ObjTotest(16,0) = "Persits.Jpeg"
	ObjTotest(16,1) = "(Persits.Jpeg图片拼合)"
ObjTotest(17,0) = "JMail.SmtpMail"
	ObjTotest(17,1) = "(Dimac JMail 邮件收发)"
ObjTotest(18,0) = "CDONTS.NewMail"
	ObjTotest(18,1) = "(虚拟 SMTP 发信)"
ObjTotest(19,0) = "Persits.MailSender"
	ObjTotest(19,1) = "(ASPemail 发信)"
ObjTotest(20,0) = "SMTPsvg.Mailer"
	ObjTotest(20,1) = "(ASPmail 发信)"
ObjTotest(21,0) = "DkQmail.Qmail"
	ObjTotest(21,1) = "(dkQmail 发信)"
ObjTotest(22,0) = "Geocel.Mailer"
	ObjTotest(22,1) = "(Geocel 发信)"
ObjTotest(23,0) = "IISmail.Iismail.1"
	ObjTotest(23,1) = "(IISmail 发信)"
ObjTotest(24,0) = "SmtpMail.SmtpMail.1"
	ObjTotest(24,1) = "(SmtpMail 发信)"
	
ObjTotest(25,0) = "SoftArtisans.ImageGen"
	ObjTotest(25,1) = "(SA 的图像读写组件)"
ObjTotest(26,0) = "W3Image.Image"
	ObjTotest(26,1) = "(Dimac 的图像读写组件)"

public IsObj,VerObj

'检查预查组件支持情况及版本

dim i
for i=0 to 25
	on error resume next
	IsObj=false
	VerObj=""
	dim TestObj
	set TestObj=server.CreateObject(ObjTotest(i,0))
	If -2147221005 <> Err then		'感谢网友的宝贵建议
		IsObj = True
		VerObj = TestObj.version
		if VerObj="" or isnull(VerObj) then VerObj=TestObj.about
	end if
	ObjTotest(i,2)=IsObj
	ObjTotest(i,3)=VerObj
next

'检查组件是否被支持及组件版本的子程序
sub ObjTest(strObj)
	on error resume next
	IsObj=false
	VerObj=""
	dim TestObj
	set TestObj=server.CreateObject (strObj)
	If -2147221005 <> Err then		'感谢网友5757的宝贵建议
		IsObj = True
		VerObj = TestObj.version
		if VerObj="" or isnull(VerObj) then VerObj=TestObj.about
	end if	
End sub
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>CMS系統</title>
<meta name="keywords" content="CMS系統">
<meta name="MSSmartTagsPreventParsing" content="TRUE" >
<link rel="shortcut icon" href="/images/favicon.ico">
<link rel="Bookmark" href="/images/favicon.ico">
<LINK href="/css/cms.css" type=text/css rel=stylesheet>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0"  width="100%" align="center">
  <tr> 
    <td bgcolor="#FFFFFF"> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" background="/images/tomenubg.gif" style="border-collapse: collapse">
          <tr class=backs height=18>
            <td align=center ><strong> 　服务器组件详细信息　</strong></td>
          </tr>
      </table>
        <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#DEEFEF" width="100%">
          <tr class=backs height=18>
            <td align=left bgcolor="#FFFFFF" > 　管理员姓名</td>
            <td bgcolor="#FFFFFF">　<%=request.cookies("member")%></td>
          </tr>
          <tr class=backs height=18>
            <td align=left bgcolor="#FFFFFF">　管理员账号</td>
            <td bgcolor="#FFFFFF">　<%=request.cookies("username")%></td>
          </tr>
          <tr class=backs height=18>
            <td align=left bgcolor="#FFFFFF">　管理员密码</td>
            <td bgcolor="#FFFFFF">　<%=request.cookies("password")%></td>
          </tr>
          <tr class=backs height=18>
            <td align=left bgcolor="#FFFFFF">　所属身份</td>
            <td bgcolor="#FFFFFF">　<%if request.cookies("vip")=1 then 
			response.write("普通代理") 
			elseif request.cookies("vip")=2 then 
			response.write("高级代理")
			elseif request.cookies("vip")=3 then 
			response.write("管理员")
			elseif request.cookies("vip")=4 then 
			response.write("超级管理员")
			end if
			%></td>
          </tr>
          
          <tr class=backs height=18>
            <td align=left bgcolor="#FFFFFF" >　末次登陆IP</td>
            <td bgcolor="#FFFFFF">　<%=Request.ServerVariables("remote_addr")%></td>
          </tr>
          <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF" > 　服务器名</td>
            <td bgcolor="#FFFFFF">　<%=Request.ServerVariables("SERVER_NAME")%></td>
          </tr>
          <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　服务器IP</td>
             
            <td bgcolor="#FFFFFF">　<%=Request.ServerVariables("LOCAL_ADDR")%></td>
         </tr>
         <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　服务器端口</td>
            <td bgcolor="#FFFFFF">　<%=Request.ServerVariables("SERVER_PORT")%></td>
         </tr>
         <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　服务器时间</td>
            <td bgcolor="#FFFFFF">　<%=now%></td>
         </tr>
         <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　IIS版本</td>
            <td bgcolor="#FFFFFF">　<%=Request.ServerVariables("SERVER_SOFTWARE")%></td>
         </tr>
         <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　脚本超时时间</td>
            <td bgcolor="#FFFFFF">　<%=Server.ScriptTimeout%> 秒</td>
         </tr>
         <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　本文件路径</td>
            <td bgcolor="#FFFFFF">　<%=server.mappath(Request.ServerVariables("SCRIPT_NAME"))%></td>
        </tr>
        <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　服务器CPU数量</td>
            <td bgcolor="#FFFFFF">　<%=Request.ServerVariables("NUMBER_OF_PROCESSORS")%> 个</td>
       </tr>
       <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　服务器解译引擎</td>
            <td bgcolor="#FFFFFF">　<%=ScriptEngine & "/"& ScriptEngineMajorVersion &"."&ScriptEngineMinorVersion&"."& ScriptEngineBuildVersion %></td>
      </tr>
      <tr class=backs height=18> 
            <td align=left bgcolor="#FFFFFF">　服务器操作系统</td>
            <td bgcolor="#FFFFFF">　<%=Request.ServerVariables("OS")%></td>
      </tr>
      <tr class=backs height=18>
        <td colspan="2" align=left bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="0" cellspacing="0" background="/images/tomenubg.gif" style="border-collapse: collapse">
          <tr class=backs height=18>
            <td height="26" align=center ><strong>系统空间占用</strong></td>
          </tr>
        </table></td>
      </tr>
      <tr class=backs height=18>
        <td height="221" colspan="2" align=left bgcolor="#FFFFFF">
        

  <table width=100% align="center" cellpadding=0 cellspacing=1  >
    <tr>
    <td bgcolor="#FFFFFF"><%
Sub ShowSpaceInfo(drvpath)
	Dim fso, d, Size, showsize
	Set fso = server.CreateObject("scripting.filesystemobject")
	drvpath = server.mappath(drvpath)
	Set d = fso.GetFolder(drvpath)
	Size = d.Size
	showsize = Size & "&nbsp;Byte"
	If Size>1024 Then
		Size = (Size \ 1024)
		showsize = Size & "&nbsp;KB"
	End If
	If Size>1024 Then
		Size = (Size / 1024)
		showsize = FormatNumber(Size, 2) & "&nbsp;MB"
	End If
	If Size>1024 Then
		Size = (Size / 1024)
		showsize = FormatNumber(Size, 2) & "&nbsp;GB"
	End If
	response.Write "<font face=verdana>" & showsize & "</font>"
End Sub

Sub Showspecialspaceinfo(method)
	Dim fso, d, fc, f1, Size, showsize, drvpath
	Set fso = server.CreateObject("scripting.filesystemobject")
	drvpath = server.mappath("pic")
	drvpath = Left(drvpath, (instrrev(drvpath, "\") -1))
	Set d = fso.GetFolder(drvpath)

	If method = "All" Then
		Size = d.Size
	ElseIf method = "Program" Then
		Set fc = d.Files
		For Each f1 in fc
			Size = Size + f1.Size
		Next
	End If

	showsize = Size & "&nbsp;Byte"
	If Size>1024 Then
		Size = (Size \ 1024)
		showsize = Size & "&nbsp;KB"
	End If
	If Size>1024 Then
		Size = (Size / 1024)
		showsize = FormatNumber(Size, 2) & "&nbsp;MB"
	End If
	If Size>1024 Then
		Size = (Size / 1024)
		showsize = FormatNumber(Size, 2) & "&nbsp;GB"
	End If
	response.Write "<font face=verdana>" & showsize & "</font>"
End Sub

Function Drawbar(drvpath)
	Dim fso, drvpathroot, d, Size, TotalSize, barsize
	Set fso = server.CreateObject("scripting.filesystemobject")
	drvpathroot = server.mappath("pic")
	drvpathroot = Left(drvpathroot, (instrrev(drvpathroot, "\") -1))
	Set d = fso.GetFolder(drvpathroot)
	TotalSize = d.Size

	drvpath = server.mappath(drvpath)
	Set d = fso.GetFolder(drvpath)
	Size = d.Size

	barsize = CInt((Size / TotalSize) * 400)
	Drawbar = barsize
End Function

Function Drawspecialbar()
	Dim fso, drvpathroot, d, fc, f1, Size, TotalSize, barsize
	Set fso = server.CreateObject("scripting.filesystemobject")
	drvpathroot = server.mappath("pic")
	drvpathroot = Left(drvpathroot, (instrrev(drvpathroot, "\") -1))
	Set d = fso.GetFolder(drvpathroot)
	TotalSize = d.Size

	Set fc = d.Files
	For Each f1 in fc
		Size = Size + f1.Size
	Next

	barsize = CInt((Size / TotalSize) * 400)
	Drawspecialbar = barsize
End Function

%>
      <blockquote>
      <%
fsoflag = 1
If fsoflag = 1 Then

%>
      <br>
      <%Function GetPP
	Dim s
	s = Request.ServerVariables("path_translated")
	GetPP = Left(s, instrrev(s, "\", Len(s)))
End Function

If sPP = "" Then sPP = GetPP
If Right(sPP, 1)<>"\" Then sPP = sPP&"\"
Set fso = server.CreateObject("scripting.filesystemobject")
Set f = fso.GetFolder(sPP)
Set fc = f.SubFolders
i = 1
i2 = 1
For Each f in fc
%>
      目录<b><%=f.name%></b>占用空间：&nbsp;<img src="/images/bar.gif" width=<%=drawbar(""&f.name&"")%> height=10>&nbsp;
      <%showSpaceinfo(""&f.name&"")%>
      <br>
      <br>
      <%i = i + 1
If i2<10 Then
	i2 = i2 + 1
Else
	i2 = 1
End If
Next
%>
      程序文件占用空间：&nbsp;<img src="/images/bar.gif" width=<%=drawspecialbar%> height=10>&nbsp;
      <%showSpecialSpaceinfo("Program")%>
      <br>
      <br>
      系统占用空间总计：<br>
      <img src="/images/bar.gif" width=400 height=10>
      <%showspecialspaceinfo("All")%>
      <%
Else
	response.Write "<br><li>本功能已经被关闭"
End If

%>
    </blockquote></td>
  </tr>
</table>
<tr>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" background="/images/tomenubg.gif" style="border-collapse: collapse">
    <tr class=backs height=18>
      <td height="26" align=center ><%
Dim strClass
	strClass = Trim(Request.Form("classname"))
	If "" <> strClass then
	Response.Write "<br>您指定的组件的检查结果："
	ObjTest(strClass)
	  If Not IsObj then 
		Response.Write "<br><font color=red>很遗憾，该服务器不支持 " & strclass & " 组件！</font>"
	  Else
		Response.Write "<br><font class=fonts>恭喜！该服务器支持 " & strclass & " 组件。该组件版本是：" & VerObj & "</font>"
	  End If
	  Response.Write "<br>"
	end if
	%>
        IIS自带的ASP组件 </td>
      </tr>
  </table>
  <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#DEEFEF" style="border-collapse: collapse">
    <tr height=18 class=backs align=center> 
      <td width=320 bgcolor="#FFFFFF">组 件 名 称</td>
      <td width=130 bgcolor="#FFFFFF">支持及版本</td>
      </tr>
    <%For i=0 to 10%>
    <tr height="18" class=backq> 
      <td align=left bgcolor="#FFFFFF">　<%=ObjTotest(i,0) & "<font color=#888888>" & ObjTotest(i,1)%></td>
      <td align=center bgcolor="#FFFFFF"> 
        <%
		If Not ObjTotest(i,2) Then 
			Response.Write "<font color=red><b>×</b></font>"
		Else
			Response.Write "<font class=fonts><b>√</b></font> <a title='" & ObjTotest(i,3) & "'>" & left(ObjTotest(i,3),11) & "</a>"
		End If%>
        </td>
      </tr>
    <%next%>
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" background="/images/tomenubg.gif" style="border-collapse: collapse">
    <tr class=backs height=18>
      <td align=center >■ 常见的文件上传和管理组件 </td>
      </tr>
  </table>
  <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#DEEFEF" width="100%">
    <tr height=18 class=backs align=center> 
      <td width=320 bgcolor="#FFFFFF">组 件 名 称</td>
      <td width=130 bgcolor="#FFFFFF">支持及版本</td>
      </tr>
    <%For i=11 to 15%>
    <tr height="18" class=backq> 
      <td align=left bgcolor="#FFFFFF">　<%=ObjTotest(i,0) & "<font color=#888888>" & ObjTotest(i,1)%></td>
      <td align=center bgcolor="#FFFFFF"> 
        <%
		If Not ObjTotest(i,2) Then 
			Response.Write "<font color=red><b>×</b></font>"
		Else
			Response.Write "<font class=fonts><b>√</b></font> <a title='" & ObjTotest(i,3) & "'>" & left(ObjTotest(i,3),11) & "</a>"
		End If%>
        </td>
      </tr>
    <%next%>
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" background="/images/tomenubg.gif" style="border-collapse: collapse">
    <tr class=backs height=18>
      <td align=center >■ 常见的收发邮件组件 </td>
      </tr>
  </table>
  <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#DEEFEF" width="100%">
    <tr height=18 class=backs align=center> 
      <td width=320 bgcolor="#FFFFFF">组 件 名 称</td>
      <td width=130 bgcolor="#FFFFFF">支持及版本</td>
      </tr>
    <%For i=16 to 23%>
    <tr height="18" class=backq> 
      <td align=left bgcolor="#FFFFFF">　<%=ObjTotest(i,0) & "<font color=#888888>" & ObjTotest(i,1)%></td>
      <td align=center bgcolor="#FFFFFF"> 
        <%
		If Not ObjTotest(i,2) Then 
			Response.Write "<font color=red><b>×</b></font>"
		Else
			Response.Write "<font class=fonts><b>√</b></font> <a title='" & ObjTotest(i,3) & "'>" & left(ObjTotest(i,3),11) & "</a>"
		End If%>
        </td>
      </tr>
    <%next%>
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" background="/images/tomenubg.gif" style="border-collapse: collapse">
    <tr class=backs height=18>
      <td align=center >■ 图像处理组件 </td>
      </tr>
  </table>
  <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#DEEFEF" width="100%">
    <tr height=18 class=backs align=center> 
      <td width=320 height="18" bgcolor="#FFFFFF">组 件 名 称</td>
      <td width=130 height="18" bgcolor="#FFFFFF">支持及版本</td>
      </tr>
    <%For i=24 to 25%>
    <tr height="18" class=backq> 
      <td align=left bgcolor="#FFFFFF">　<%=ObjTotest(i,0) & "<font color=#888888>" & ObjTotest(i,1)%></td>
      <td height="18" align=center bgcolor="#FFFFFF"><%
		If Not ObjTotest(i,2) Then 
			Response.Write "<font color=red><b>×</b></font>"
		Else
			Response.Write "<font class=fonts><b>√</b></font> <a title='" & ObjTotest(i,3) & "'>" & left(ObjTotest(i,3),11) & "</a>"
		End If%>
        </td>
      </tr>
    <%next%>
  </table>
  <br>
  <font color="#000000" class=fonts>&nbsp;&nbsp;其他组件支持情况检测</font><br>
  &nbsp;&nbsp;在下面的输入框中输入你要检测的组件的ProgId或ClassId。 
  <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#DEEFEF" width="100%">
    <form action=<%=Request.ServerVariables("SCRIPT_NAME")%> method=post id=form1 name=form1>
      <tr height="18" class=backq> 
        <td align=center height=30> 
          <input class=input type=text value="" name="classname" size=40>
          <input type=submit value=" 确 定 " class=backc id=submit1 name=submit1>
          <input type=reset value=" 重 填 " class=backc id=reset1 name=reset1>
          </td>
        </tr>
      </form>
  </table>
  <br>
  <font class=fonts>&nbsp;&nbsp;ASP脚本解释和运算速度测试</font><br>
  &nbsp;&nbsp;我们让服务器执行50万次“1＋1”的计算，记录其所使用的时间。 
  <table class=backq border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#DEEFEF" width="100%">
    <tr height=18 class=backs align=center> 
      <td width=351 bgcolor="#FFFFFF">服务器</td>
      <td width=142 bgcolor="#FFFFFF">完成时间</td>
      </tr>
    <form action="<%=Request.ServerVariables("SCRIPT_NAME")%>" method=post>
      <%

	'感谢网际同学录 http://www.5757.net 推荐使用timer函数
	'因为只进行50万次计算，所以去掉了是否检测的选项而直接检测
	
	dim t1,t2,lsabc,thetime
	t1=timer
	for i=1 to 500000
		lsabc= 1 + 1
	next
	t2=timer

	thetime=cstr(int(( (t2-t1)*10000 )+0.5)/10)
%>
      <tr height=18> 
        <td width="351" align=left bgcolor="#FFFFFF"><font color=red>&nbsp;&nbsp;您正在使用的这台服务器</font></td>
        <td width="142" bgcolor="#FFFFFF"><font color=red>&nbsp;&nbsp;<%=thetime%> 毫秒</font></td>
        </tr>
      </form>
  </table>
    </body>
</html>